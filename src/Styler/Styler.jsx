import React, { useState } from 'react';
import StylerButton from './StylerButton';
import { StylerDisplay } from './StylerDisplay';
import './Styler.css';

export default function Styler(props) {
    const [colors, setColors] = useState({
        text: 'white',
        background: 'darkblue'
    });

    const changeColors = (backgroundColor, textColor) => {
        setColors({
            text: textColor,
            background: backgroundColor
        })
    }
        
    return (
        <div className="Styler">
            <StylerDisplay text={props.text} backgroundColor={colors.background} textColor={colors.text}></StylerDisplay>
            <StylerButton backgroundColor="Green" textColor="White" onClick={changeColors}></StylerButton>
            <StylerButton backgroundColor="Blueviolet" textColor="White" onClick={changeColors}></StylerButton>
            <StylerButton backgroundColor="Orange" textColor="Black" onClick={changeColors}></StylerButton>
        </div>
    );
}