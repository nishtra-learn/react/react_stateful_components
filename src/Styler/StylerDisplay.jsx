import React from 'react';

export function StylerDisplay(props) {
    const styleObj = {
        backgroundColor: props.backgroundColor,
        color: props.textColor
    }

    return (
        <div style={styleObj} className="StylerDisplay">{props.text}</div>
    );
}

StylerDisplay.defaultProps = {
    backgroundColor: 'darkblue',
    textColor: 'white',
    text: 'Hello, world'
}