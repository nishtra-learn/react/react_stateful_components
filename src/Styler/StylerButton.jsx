import React from 'react';

export default function StylerButton(props) {
    const styleObj = {
        backgroundColor: props.backgroundColor.toLowerCase(),
        color: props.textColor.toLowerCase()
    }

    const clickHandler = () => {
        props.onClick(styleObj.backgroundColor, styleObj.color);
    }

    return (
        <button style={styleObj} onClick={clickHandler}>
            <div>Background: {props.backgroundColor}</div>
            <div>Text: {props.textColor}</div>
        </button>
    );
}