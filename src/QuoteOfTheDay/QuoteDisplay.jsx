import React from 'react';

export default function QuoteDisplay(props) {
    return (
        <div className="QuoteDisplay">
            <div>{props.text}</div>
            <div className="divider"></div>
            <div>— {props.author}</div>
        </div>
    );
}