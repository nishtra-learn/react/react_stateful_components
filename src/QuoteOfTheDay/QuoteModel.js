export class QuoteModel {
    constructor(text = '', author = '') {
        this.text = text;
        this.author = author;
    }
}