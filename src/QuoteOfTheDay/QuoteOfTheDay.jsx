import React, { useState } from 'react';
import QuoteDisplay from './QuoteDisplay';
import { QuoteModel } from "./QuoteModel";
import './QuoteDisplay.css';

export default function QuoteOfTheDay() {
    const quotes = [
        new QuoteModel('I have a bad feeling about this', 'Luke Skywalkeer'),
        new QuoteModel('With great power comes great responsibility', 'Uncle Ben'),
        new QuoteModel('Why so serious?', 'The Joker'),
        new QuoteModel('We stopped checking for monsters under our bed, when we realized they were inside us.', 'The Joker'),
        new QuoteModel('Two things are infinite: the universe and human stupidity; and I\'m not sure about the universe.', 'Albert Einstein'),
        new QuoteModel('Be yourself; everyone else is already taken.', 'Oscar Wilde')
    ];

    function getRandomQuoteIndex() { 
        return Math.floor(Math.random() * quotes.length);
    }
    const [quoteIndex, setQuoteIndex] = useState(getRandomQuoteIndex());
    function selectRandom() { 
        const randomQuoteIndex = getRandomQuoteIndex();
        setQuoteIndex(randomQuoteIndex);
    };
    
    return (
        <div className="QuoteOfTheDay">
            <QuoteDisplay text={quotes[quoteIndex].text} author={quotes[quoteIndex].author}></QuoteDisplay>
            <button onClick={selectRandom}>Get new quote</button>
        </div>
    );
}