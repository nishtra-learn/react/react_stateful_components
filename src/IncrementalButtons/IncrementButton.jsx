import React from 'react';

export default function IncrementButton(props) {
    const btnText = props.increment >= 0 ? `+${props.increment}`: `${props.increment}`;
    const clickHandler = (incVal) => {
        props.onClick(props.increment);
    }

    return (
        <button onClick={clickHandler}>{btnText}</button>
    );
}