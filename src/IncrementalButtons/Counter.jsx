import React, { useState } from 'react';
import CounterDisplay from './CounterDisplay';
import IncrementButton from './IncrementButton';
import './Counter.css';

export default function Counter() {
    const [value, setValue] = useState(0);
    const incValue = (inc) => {
        setValue(value + inc);
    };

    return (
        <div className="Counter">
            <CounterDisplay value={value}></CounterDisplay>
            <IncrementButton increment={10} onClick={incValue}></IncrementButton>
            <IncrementButton increment={-100} onClick={incValue}></IncrementButton>
            <IncrementButton increment={25} onClick={incValue}></IncrementButton>
        </div>
    );
}