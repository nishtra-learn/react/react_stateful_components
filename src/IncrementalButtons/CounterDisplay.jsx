import React from 'react';

export default function CounterDisplay(props) {

    return (
        <h3>{props.value}</h3>
    );
}

CounterDisplay.defaultProps = {
    value: 0
}