import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Counter from './IncrementalButtons/Counter';
import Styler from './Styler/Styler';
import StylerClassBased from './StylerClassBased/StylerClassBased';
import QuoteOfTheDay from './QuoteOfTheDay/QuoteOfTheDay';

ReactDOM.render(
  <React.StrictMode>
    <div>
      <h3>Counter</h3>
      <Counter></Counter>
    </div>
    <hr/>
    <div>
      <h3>Functional component based styler</h3>
      <Styler text="This is text"></Styler>
    </div>
    <hr/>
    <div>
      <h3>Class component based styler</h3>
      <StylerClassBased></StylerClassBased>
    </div>
    <hr/>
    <div>
      <h3>Quote of the day</h3>
      <QuoteOfTheDay></QuoteOfTheDay>
    </div>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
