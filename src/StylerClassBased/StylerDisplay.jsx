import React from 'react';

export default class StylerDisplay extends React.Component {
    static defaultProps = {
        backgroundColor: 'darkblue',
        textColor: 'white',
        text: 'Hello, world'
    }

    render() {
        const styleObj = {
            backgroundColor: this.props.backgroundColor,
            color: this.props.textColor
        }

        return (
            <div style={styleObj} className="StylerDisplay" > { this.props.text}</div>
        );
    }
}