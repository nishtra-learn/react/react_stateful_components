import React from 'react';

export default class StylerButton extends React.Component {
    static defaultProps = {
        backgroundColor: 'default',
        textColor: 'black'
    }

    render() {
        const styleObj = {
            backgroundColor: this.props.backgroundColor.toLowerCase(),
            color: this.props.textColor.toLowerCase()
        }

        const clickHandler = () => {
            this.props.onClick(styleObj.backgroundColor, styleObj.color);
        }

        return (
            <button style={styleObj} onClick={clickHandler}>
                <div>Background: {this.props.backgroundColor}</div>
                <div>Text: {this.props.textColor}</div>
            </button>
        );
    }
}