import React from 'react';
import StylerButton from './StylerButton';
import StylerDisplay from './StylerDisplay';
import './StylerClassBased.css';

export default class StylerClassBased extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            colors: {
                text: 'black',
                background: 'green'
            }
        };
    }

    render() {
        const changeColors = (backgroundColor, textColor) => {
            this.setState({
                colors: {
                    text: textColor,
                    background: backgroundColor
                }
            })
        }

        return (
            <div className="StylerClassBased">
                <StylerDisplay text={this.props.text} backgroundColor={this.state.colors.background} textColor={this.state.colors.text}></StylerDisplay>
                <StylerButton backgroundColor="Darkblue" textColor="White" onClick={changeColors}></StylerButton>
                <StylerButton backgroundColor="Green" textColor="Orange" onClick={changeColors}></StylerButton>
                <StylerButton backgroundColor="Cyan" textColor="Magenta" onClick={changeColors}></StylerButton>
            </div>
        );
    }
}